# -*- coding:utf-8 -*-
from datetime import datetime
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
import pytz
import re


def ago(time, timezone='Asia/Seoul'):
    response = ''
    result = datetime.now(pytz.UTC) - time
    seconds = int(result.total_seconds())
    days = result.days
    if seconds < 60:
        response = str(seconds) + '초전'
    elif seconds < 3600:
        response = str(seconds / 60) + '분전'
    elif seconds < 86400:
        response = str(seconds / 3600) + '시간전'
    elif days <= 31:
        response = str(days) + '일전'
    else:
        response = time.astimezone(timezone).strftime('%Y-%m-%d %H:%M')
        # stamp = mktime(result.timetuple())
        # response = format_date_time(stamp)
    return response


