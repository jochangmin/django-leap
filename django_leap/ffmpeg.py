# -*- encoding:utf-8 -*-
'''
Created on Oct 9, 2013

@author: a141890
'''

import json
import os
import re
import subprocess


class LeapFFBase(object):
    def __init__(self, inputPath, outputPath=None):
        if type(inputPath) == list or type(inputPath) == tuple:
            self._input = inputPath
        else:
            self._input = [inputPath]
        self._output = outputPath
        self._info = None
        self._sentence = {}

    def clear(self):
        self._sentence = {}
        return self

    def _sentenceToArray(self):
        response = []
        for k, v in self._sentence.items():
            response.append(k)
            if v: response.append(v)
        return response

    def execute(self):
        raise Exception("The method must be overriden before it can be used")


class LeapFFProbe(LeapFFBase):
    PRINT_FORMAT_COMPACT = "compact"
    PRINT_FORMAT_CSV = "csv"
    PRINT_FORMAT_FLAT = "flat"
    PRINT_FORMAT_INI = "ini"
    PRINT_FORMAT_JSON = "json"
    PRINT_FORMAT_xml = "xml"

    def __init__(self, inputPath):
        LeapFFBase.__init__(self, inputPath, outputPath=None)
        self._isFormatJson = False
        self.clear();

    def clear(self):
        LeapFFBase.clear(self)
        return self

    def print_format(self, format=None):
        """
        -print_format format  set the output printing format 
        (available formats are: default, compact, csv, flat, ini, json, xml)
        
        the default format is json
        """
        if not format: format = LeapFFProbe.PRINT_FORMAT_JSON
        self._sentence['-print_format'] = format

        if format == LeapFFProbe.PRINT_FORMAT_JSON:
            self._isFormatJson = True
        return self

    def show_format(self):
        self._sentence['-show_format'] = None
        return self

    def show_streams(self):
        self._sentence['-show_streams'] = None
        return self

    def show_frames(self):
        """
        It's so fucking huge. 
        It shows every single frame information of the whole video 
        """
        self._sentence['-show_frames'] = None
        return self

    def execute(self):
        """
        @return: the information of the video in json
        """
        command = ['ffprobe', '-v', 'quiet']
        for inp in self._input:
            command.append('-i')
            command.append(inp)

        command.extend(self._sentenceToArray())
        # print "LeapFFProbe execute :", command
        pipe = subprocess.Popen(command, stdout=subprocess.PIPE)
        result = pipe.stdout.read()
        if self._isFormatJson:
            return json.loads(result)
        return result


class LeapFFMpegBase(LeapFFBase):
    def __init__(self, *args, **kwargs):
        super(LeapFFMpegBase, self).__init__(*args, **kwargs)
        self._useResize = False
        self._adaptiveResize = True


    def aspect_ratio(self, ratio):
        """
        set aspect ratio (4:3, 16:9 or 1.3333, 1.7777)
        This does not crop the video but compress the video. 
        """
        self._sentence['-aspect'] = ratio
        return self

    def bitrate_for_video(self, bitrate):
        """
        @param bitrate : the bitrate of the output video
        200k : too much low quality
        360k : I think it's ok for mobile
        """
        self._sentence['-b'] = bitrate
        return self

    def cut(self, cuttingTime):
        """
        @param cuttingTime: the duration time of output video
        the time specified is in seconds. but the format "hh.mm.ss" is also supported
        """
        self._sentence['-t'] = str(cuttingTime)
        return self

    def codec_for_video(self, codec="h264"):
        """
        force video codec ('copy' to copy stream)
        copy
        h264
        mpeg4
        """
        self._sentence['-vcodec'] = codec
        return self

    def codec_for_audio(self, codec):
        """
        force audio codec ('copy' to copy stream)
        copy
        mp3
        """
        self._sentence['-acodec'] = codec
        return self

    def format(self, format="mp4"):
        """
        """
        self._sentence['-f'] = format
        return self

    def frames(self, frameCount):
        """
        set the number of video frames to record
        """
        self._sentence['-vframes'] = str(frameCount)
        return self

    def overrite(self):
        self._sentence['-y'] = None
        return self

    def resolution(self, width, height):
        """
        set frame size (WxH or abbreviation)
        """
        self._sentence['-s'] = str(width) + "x" + str(height)
        return self

    def resize(self, reqWidth=None, reqHeight=-1, keepAspect=True, adaptiveResize=True):
        """
        Be careful before using this method. 
        sometimes the method is the culprit who crashes down the encoding. 
        use even number for reqWidth and reqHeight. ex) 32, 64, 128, 256, 320 something like this.
        """
        self._useResize = True
        self._adaptiveResize = adaptiveResize
        self._reqWidth = reqWidth

        if not reqWidth and not reqHeight:
            return self

        print " ".join(self._sentence)
        if not keepAspect:
            self.resolution(reqWidth, reqHeight)
        else:
            self.get_info()
            # the actual video size
            videoWidth = self._info['width']
            videoHeight = self._info['height']

            if reqHeight <= 0:
                reqHeight = videoHeight

            # the request size should not be bigger than original size
            if not reqWidth or reqWidth > videoWidth: reqWidth = videoWidth
            if not reqHeight or reqHeight > videoHeight: reqHeight = videoHeight

            # check if the resizing is needed
            if reqWidth == videoWidth and reqHeight == videoHeight:
                return self

            # Get relative size of the reqWidth and the reqHeight respectively
            relativeWidth = videoWidth * reqHeight / videoHeight
            relativeHeight = videoHeight * reqWidth / videoWidth

            # print "videoWidth ", videoWidth
            #print "videoHeight ", videoHeight
            #print "reqWidth ", reqWidth
            #print "reqHeight ", reqHeight
            #print "relativeWidth ", relativeWidth
            #print "relativeHeight ", relativeHeight

            if reqWidth != videoWidth:  # reqHeight has something wrong
                #print "reqHeight is wrong -> ", reqWidth, relativeHeight
                self.resolution(reqWidth, relativeHeight)
            else:
                #print "reqWidth is wrong -> ", relativeWidth, reqHeight
                self.resolution(relativeWidth, reqHeight)
        return self


    def rotate(self, degree):
        """
        It even rotates the video to 45 degree. WOW.
        """
        self._sentence['-vf'] = str(degree)

    def start_time(self, startTime):
        """
        set the start time offset
        HH:mm:ss.SS is supported
        """
        self._sentence['-ss'] = str(startTime)
        return self

    def scale(self, width, height, keepRatio=True):
        """
        scale is like resize method. 
        But this method is processed internally. 
        That is, the height size is calculated by the ffmpeg automatically.
        On the other hand, resize method calculate height by this class. 
        
        This method conflicts with rotate method, which use -vf option
        
        like resize method, this method sometimes does not work when the width value is like
        720 or 400 something like this. I don't know why, but
        just simply use 800 or 380 width then no exception will be raised. 
        
        NOT WORK : 720, 400
        WORK :     800, 380
        """
        scale = 'scale=' + str(width) + ":"
        if keepRatio:
            scale += "-1"
        else:
            scale += str(height)

        self._sentence['-vf'] = scale
        return self

    def volume(self, volume):
        """
        change audio volume (256=normal)
        if the volume level is more than 256, the sound could be little bit louder but noisy
        """
        self._sentence['-vol'] = str(volume)
        return self

    def get_info(self):
        if self._info:
            return self._info

        response = {}
        tempSentence = self._sentence
        tempOutput = self._output
        self.clear()

        self._output = "/dev/null"
        self.codec_for_audio("copy")
        self.codec_for_video("copy")
        self.format("null")
        content = self.format()._execute(quiet=False)
        start = int(re.search("Metadata", content).start())
        content = content[start:]
        # frameCount = int(re.search("frame= *(\d+)", content).group(1))
        bitrate = int(re.search("bitrate: (\d+)", content).group(1))

        duration = re.search("Duration: ((?P<hours>\d\d):(?P<mins>\d\d):(?P<secs>\d\d)\.(?P<mili>\d\d))", content)
        hours = int(duration.group('hours'))
        mins = int(duration.group('mins'))
        secs = int(duration.group('secs'))
        milisecs = float(duration.group('mili'))

        probeJson = LeapFFProbe(self._input).show_streams().print_format().execute()
        streamJson = probeJson['streams'][0]
        width = streamJson['width']
        height = streamJson['height']

        #response['frame_count'] = frameCount
        response['bitrate'] = bitrate
        response['duration'] = hours * 60. * 60. + mins * 60. + secs + milisecs * 0.01
        response['duration_string'] = duration.group(1)
        response['duration_hours'] = hours
        response['duration_mins'] = mins
        response['duration_secs'] = secs
        response['duration_milisecs'] = milisecs
        response['width'] = width
        response['height'] = height

        self._output = tempOutput
        self._sentence = tempSentence
        self._info = response
        return response

    def _execute(self, quiet=True):
        command = ['ffmpeg', '-y']
        for inp in self._input:
            command.append('-i')
            command.append(inp)

        if quiet:
            command.append('-v')
            command.append('quiet')
        command.extend(self._sentenceToArray())
        command.append(self._output)

        print " ".join(command)
        pipe = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        return pipe.stdout.read()

    def execute(self, quiet=True):
        if not self._useResize or not self._adaptiveResize:
            return self._execute(quiet)

        result = None
        count = 0
        while True:
            print "execute: ", self._reqWidth, count
            result = self.resize(self._reqWidth, -1)._execute(quiet)
            self._reqWidth += 1
            count += 1

            if count >= 10000:
                return result

            if os.path.exists(self._output):
                if os.stat(self._output).st_size <= 0:
                    os.remove(self._output)
                    continue
                return result


class LeapFFMpeg(LeapFFMpegBase):
    """
    The class has preset methods
    """

    def _calculate_frame_time(self, frameNumber=None):
        info = self.get_info()
        frameCount = info['frame_count']
        duration = info['duration']

        if not frameNumber:
            return float("{0:.7f}".format(float(int(duration)) / frameCount))
        return float("{0:.7f}".format(float(int(duration)) / frameCount)) * (frameNumber - 1)

    def capture_image_by_time(self, frameTime=None):
        # print 'capture_image_by_time'
        """
        @param captureTime: seconds or HH:mm:ss.SS 
        """
        self.format("image2")
        self.frames(1)
        if frameTime: self.start_time(frameTime)
        return self

    def capture_image(self, frameNumber):
        # print 'capture_image'
        frameTime = self._calculate_frame_time(frameNumber)

        #print "duration/frameCount :"+ str(duration/frameCount)
        #print "duration :"+ str(duration)
        print "captureTime :" + str(frameTime)
        self.capture_image_by_time(frameTime)
        return self

    def mute(self):
        return self.volume(0)


if __name__ == "__main__":
    """
    print os.getcwd()
    mketProbe = LeapFFProbe("birth")
    mketProbe.print_format(LeapFFProbe.PRINT_FORMAT_JSON)
    responseJson = mketProbe.show_streams().execute()
    pprint(responseJson)
    
    print LeapFFMpeg.get_info("movie.mp4")
    
    for i in xrange(100):
        mketff = LeapFFMpeg("movie.mp4", "xx/output%d.png" %i)
        print mketff.capture_image(0.04 * i).execute()
    """
    width = 400
    filePath = u"/home/a141890/movies/유가네.mov"
    destPath = u"/home/a141890/movies/유가네720.mp4"

    videoFFmpeg = LeapFFMpeg(filePath, destPath)
    # videoFFmpeg.cut(5).execute()
    videoFFmpeg.resize(720, -1).codec_for_video('h264').codec_for_audio('libfdk_aac').cut(60 * 20).execute(quiet=False)
