from datetime import datetime, timedelta
from django.db import models


class UplusSMSModel(models.Model):
    TR_NUM = models.AutoField(primary_key=True)
    TR_SENDDATE = models.DateTimeField(blank=True, null=True, auto_now=False)
    TR_ID = models.CharField(max_length=16, blank=True, null=True, default=None)
    TR_SENDSTAT = models.CharField(max_length=1, default="0")
    TR_RSLTSTAT = models.CharField(max_length=2, blank=True, null=True, default="00")
    TR_MSGTYPE = models.CharField(max_length=1, default="0")
    TR_PHONE = models.CharField(max_length=20)
    TR_CALLBACK = models.CharField(max_length=20, blank=True, null=True, default=None)
    
    TR_RSLTDATE = models.DateTimeField(blank=True, null=True)
    TR_MODIFIED = models.DateTimeField(blank=True, null=True)
    TR_MSG = models.CharField(max_length=160, blank=True, null=True)
    TR_NET = models.CharField(max_length=4, blank=True, null=True)
    TR_ETC1 = models.CharField(max_length=160, blank=True, null=True)
    TR_ETC2 = models.CharField(max_length=160, blank=True, null=True)
    TR_ETC3 = models.CharField(max_length=160, blank=True, null=True)
    TR_ETC4 = models.CharField(max_length=160, blank=True, null=True)
    TR_ETC5 = models.CharField(max_length=160, blank=True, null=True)
    TR_ETC6 = models.CharField(max_length=160, blank=True, null=True)
    TR_REALSENDDATE = models.DateTimeField(blank=True, null=True)
    
    class Meta:
        #app_label = "sms"
        db_table = 'SC_TRAN'
        
    @classmethod
    def send_message(cls, toPhone, fromPhone, message):
        #newMsg.TR_NUM = models.F('TR_NUM')+1; # <- do not use
        newMsg = cls()
        newMsg.TR_SENDDATE = datetime.now() 
        newMsg.TR_SENDSTAT = "0"
        newMsg.TR_MSGTYPE = "0"
        newMsg.TR_PHONE = toPhone
        newMsg.TR_CALLBACK = fromPhone
        newMsg.TR_MSG = message
        newMsg.save()
        #newMsg.save(using="sms");
        



"""
+-----------------+--------------+------+-----+---------+----------------+
| Field           | Type         | Null | Key | Default | Extra          |
+-----------------+--------------+------+-----+---------+----------------+
| TR_NUM          | bigint(20)   | NO   | PRI | NULL    | auto_increment |
| TR_SENDDATE     | datetime     | YES  | MUL | NULL    |                |
| TR_ID           | varchar(16)  | YES  |     | NULL    |                |
| TR_SENDSTAT     | varchar(1)   | NO   | MUL | 0       |                |
| TR_RSLTSTAT     | varchar(2)   | YES  |     | 00      |                |
| TR_MSGTYPE      | varchar(1)   | NO   |     | 0       |                |
| TR_PHONE        | varchar(20)  | NO   | MUL |         |                |
| TR_CALLBACK     | varchar(20)  | YES  |     | NULL    |                |

| TR_RSLTDATE     | datetime     | YES  |     | NULL    |                |
| TR_MODIFIED     | datetime     | YES  |     | NULL    |                |
| TR_MSG          | varchar(160) | YES  |     | NULL    |                |
| TR_NET          | varchar(4)   | YES  |     | NULL    |                |
| TR_ETC1         | varchar(160) | YES  |     | NULL    |                |
| TR_ETC2         | varchar(160) | YES  |     | NULL    |                |
| TR_ETC3         | varchar(160) | YES  |     | NULL    |                |
| TR_ETC4         | varchar(160) | YES  |     | NULL    |                |
| TR_ETC5         | varchar(160) | YES  |     | NULL    |                |
| TR_ETC6         | varchar(160) | YES  |     | NULL    |                |
| TR_REALSENDDATE | datetime     | YES  |     | NULL    |                |
+-----------------+--------------+------+-----+---------+----------------+
"""
